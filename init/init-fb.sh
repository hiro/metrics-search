#!/bin/bash

# Download recent torperf logs from collector
until wget --recursive \
     --no-clobber \
     --page-requisites \
     --convert-links \
     --domains collector.torproject.org \
     --no-parent \
     -P /var/log/collector https://collector.torproject.org/recent/torperf/; do
    echo Downloading torperf files from collector...
    sleep 1
done
rm -fr /var/log/collector/collector.torproject.org/icons
find /var/log/collector/collector.torproject.org/ -type f -name 'index.html*' -exec rm {} \;
find /var/log/collector/collector.torproject.org/ -type f -name '*.orig' -exec rm {} \;
