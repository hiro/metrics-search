# Metrics Search Operator Guide

Welcome! This project builds an Elasticsearch Stack in Docker to ingest and analyze raw data logs from various sources.

To understand how the stack is built please have a look at the docker-compose.yml file.
Here is where all the services are built. A little familiarity with Docker and docker-compose is required.

The config folders contains the configurations of the single services and beats (used to send data to Elasticsearch).

The init folder contains the script used to configure the stack at build time. These are configure-stack.sh and init-fb.sh.

The init-fb.sh files is used to download the tpf onionperf logs from https://collector.torproject.org. More logs can be added easily by adding the filebeat prospectors configs under config/beats/filebeat/prospectors.d
